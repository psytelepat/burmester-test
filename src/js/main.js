import './vendor';
import {helpers} from './common/helpers';
import './modules/sidemenu';
import './modules/scrollnav';
import './modules/videos';
import './modules/sliders';

helpers.checkMobile();
helpers.checkIE();
