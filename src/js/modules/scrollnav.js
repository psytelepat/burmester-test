import {helpers} from '../common/helpers';

function checkProgress() {
	$('.js-anchor').each((i, elem) => {
		if (helpers.isElementPartiallyInViewport(elem)) {
			let curHash = $(elem).attr('id');

			$('.sidemenu__nav a').removeClass('is-current');
			$(`.sidemenu__nav a[href="#${curHash}"]`).addClass('is-current');

			// if (location.hash !== `#${curHash}`) {
			// 	if (history.pushState) {
			// 		history.pushState(null, null, `#${curHash}`);
			// 	} else {
			// 		window.location.hash = `#${curHash}`;
			// 	}
			// }
		}
	});
}

$('.hero-more').on('click', (e) => {
	$(e.currentTarget).blur();

	TweenLite.to(window, 1, {
		scrollTo: $('.section--first').offset().top,
	});
});

$('.sidemenu__nav a').on('click', (e) => {
	let curHash = $(e.currentTarget).attr('href');

	$(e.currentTarget).blur();

	TweenLite.to(window, 2, {
		scrollTo: $(curHash).offset().top - 50,
	});

	e.preventDefault();
});

$(window).on('scroll', () => {
	checkProgress();
});
