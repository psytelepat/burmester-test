import Hammer from 'hammerjs';

export class ShimSlider {
	constructor($elem, options) {
		this.$container = $elem;
		this.$shim = $elem.find('.shim');
		this.$images = $elem.find('img');
		this.params = {
			$bntPrev: options.btnPrev,
			$bntNext: options.btnNext,
			$pager: options.pager ? options.pager : null,
			onChange: options.onChange ? options.onChange : null,
		};
		this.state = {
			totalSlides: this.$images.length,
			currentSlide: 1,
			isMoving: false,
		};

		if (this.params.$pager) {
			this.params.$pager.text(`${this.state.currentSlide} / ${this.state.totalSlides}`);
		}

		if (this.params.$bntPrev) {
			this.params.$bntPrev.on('click', (e) => {
				$(e.currentTarget).blur();
				this.prevSlide();
			});
		}

		if (this.params.$bntNext) {
			this.params.$bntNext.on('click', (e) => {
				$(e.currentTarget).blur();
				this.nextSlide();
			});
		}

		// append swipe
		let manager = new Hammer.Manager(this.$container[0]);
		let swipe = new Hammer.Swipe();

		manager.add(swipe);

		manager.on('swipe', (e) => {
			let direction = e.offsetDirection;

			if (direction === 2) {
				this.nextSlide();
			}

			if (direction === 4) {
				this.prevSlide();
			}
		});
	}

	changeSlide(slideNum, direction) {
		let self = this;
		let nextNum;

		if (this.state.isMoving) {
			return;
		}

		if (slideNum > this.state.totalSlides) {
			nextNum = 1;
		} else if (slideNum < 1) {
			nextNum = this.state.totalSlides;
		} else {
			nextNum = slideNum;
		}

		// let $currentSlide = this.$container.find(`[data-slide="${this.state.currentSlide}"]`);
		let $nextSlide = this.$container.find(`[data-slide="${nextNum}"]`);

		if (direction === 'next') {
			TweenMax.set(this.$shim, {
				x: '100%',
			});
		} else {
			TweenMax.set(this.$shim, {
				x: '-100%',
			});
		}

		new TimelineMax({
			onStart() {
				self.state.isMoving = true;

				if (self.params.onChange) {
					self.params.onChange(self.state.currentSlide, nextNum, direction);
				}
			},
			onComplete() {
				self.state.isMoving = false;
			},
		})
			.to(self.$shim, 0.4, {
				x: '0%',
			})
			.add(() => {
				self.$images.addClass('is-hidden');
				$nextSlide.removeClass('is-hidden');
				self.state.currentSlide = nextNum;

				if (self.params.$pager) {
					self.params.$pager.text(`${this.state.currentSlide} / ${this.state.totalSlides}`);
				}
			})
			.to(self.$shim, 0.4, {
				x: direction === 'next' ? '-100%' : '100%',
			});
	}

	nextSlide() {
		this.changeSlide(this.state.currentSlide + 1, 'next');
	}

	prevSlide() {
		this.changeSlide(this.state.currentSlide - 1, 'prev');
	}
}

let sliders = {};

sliders.soundSlider = new ShimSlider($('.sound-sliderblock .image-slider'), {
	btnPrev: $('.sound-sliderblock__nav .slider-btn--prev'),
	btnNext: $('.sound-sliderblock__nav .slider-btn--next'),

	onChange(currentSlide, nextSlide, direction) {
		let $dateShim = $('.sound-sliderblock__dateblock .shim');
		let $nextDate = $(`.sound-sliderblock__date[data-slide="${nextSlide}"]`);
		let $curText = $(`.sound-sliderblock__text[data-slide="${currentSlide}"]`);
		let $nextText = $(`.sound-sliderblock__text[data-slide="${nextSlide}"]`);

		new TimelineMax()
			.to($dateShim, 0.7, {
				y: '-20%',
				rotation: direction === 'next' ? 10 : -10,
				ease: Power3.EaseIn,
			})
			.to($curText, 0.3, {
				opacity: 0,
				y: direction === 'next' ? 30 : -20,
			}, '-=0.6')
			.add(() => {
				$('.sound-sliderblock__text').addClass('is-hidden');
				$('.sound-sliderblock__date').addClass('is-hidden');
				$nextDate.removeClass('is-hidden');
				$nextText.removeClass('is-hidden');

				TweenMax.set($dateShim, {
					rotation: direction === 'next' ? -10 : 10,
				});
			})
			.to($dateShim, 0.7, {
				y: '100%',
				rotation: 0,
				ease: Power3.EaseOut,
			})
			.fromTo($nextText, 0.3, {
				opacity: 0,
				y: direction === 'next' ? -20 : 30,
			}, {
				opacity: 1,
				y: 0,
			}, '-=0.6');
	},
});

sliders.carSlider = new ShimSlider($('.car-sliderblock .image-slider'), {
	btnPrev: $('.car-sliderblock__controls .slider-btn--prev'),
	btnNext: $('.car-sliderblock__controls .slider-btn--next'),
	pager: $('.car-sliderblock__controls .slider-pager'),

	onChange(currentSlide, nextSlide, direction) {
		let $curLogo = $(`.car-sliderblock__logo[data-slide="${currentSlide}"]`);
		let $nextLogo = $(`.car-sliderblock__logo[data-slide="${nextSlide}"]`);
		let $curText = $(`.car-sliderblock__text[data-slide="${currentSlide}"]`);
		let $nextText = $(`.car-sliderblock__text[data-slide="${nextSlide}"]`);

		$('.car-sliderblock__nav button').removeClass('is-current');
		$(`.car-sliderblock__nav button[data-slide="${nextSlide}"]`).addClass('is-current');

		new TimelineMax()
			.to($curLogo, 0.3, {
				opacity: 0,
			})
			.to($curText, 0.3, {
				opacity: 0,
				y: direction === 'next' ? 30 : -30,
			}, '-=0.2')
			.add(() => {
				$('.car-sliderblock__logo').addClass('is-hidden');
				$('.car-sliderblock__text').addClass('is-hidden');
				$nextLogo.removeClass('is-hidden');
				$nextText.removeClass('is-hidden');
			})
			.fromTo($nextLogo, 0.3, {
				opacity: 0,
			}, {
				opacity: 1,
			})
			.fromTo($nextText, 0.3, {
				opacity: 0,
				y: direction === 'next' ? -30 : 30,
			}, {
				opacity: 1,
				y: 0,
			}, '-=0.2');
	},
});

$('.car-sliderblock__nav button').on('click', (e) => {
	let currentSlide = $(e.currentTarget).data('slide');

	$(e.currentTarget).blur();
	sliders.carSlider.changeSlide(currentSlide, 'next');
});

sliders.bigSlider = new ShimSlider($('.big-slider__imageblock'), {
	btnPrev: $('.big-slider__nav .slider-btn--prev'),
	btnNext: $('.big-slider__nav .slider-btn--next'),
	pager: $('.big-slider__nav .slider-pager'),
});
