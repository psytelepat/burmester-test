let $sidemenu = $('.sidemenu');
let $sidemenuToggle = $('.sidemenu__toggle');
let $page = $('.page');

export let sidemenuAnim = {
	show() {
		TweenMax.killTweensOf([
			$sidemenu,
			$page,
		]);

		new TimelineMax({
			onStart() {
				$sidemenu.addClass('is-moving');
				$sidemenu.addClass('is-opened');
			},
			onComplete() {
				$sidemenu.removeClass('is-moving');
			},
		})
			.to($sidemenu, 0.5, {
				x: 0,
			})
			.to($page, 0.5, {
				x: -370,
			}, '-=0.5');
	},

	hide() {
		TweenMax.killTweensOf([
			$sidemenu,
			$page,
		]);

		new TimelineMax({
			onStart() {
				$sidemenu.addClass('is-moving');
				$sidemenu.removeClass('is-opened');
			},
			onComplete() {
				$sidemenu.removeClass('is-moving');
			},
		})
			.to($sidemenu, 0.5, {
				x: 370,
				clearProps: 'transform',
			})
			.to($page, 0.5, {
				x: 0,
				clearProps: 'transform',
			}, '-=0.5');
	},
};

$sidemenuToggle.on('click', (e) => {
	$(e.currentTarget).blur();

	if ($sidemenu.hasClass('is-opened')) {
		sidemenuAnim.hide();
	} else {
		sidemenuAnim.show();
	}
});
