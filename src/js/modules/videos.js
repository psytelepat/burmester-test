import Player from '@vimeo/player';

let videoObjects = [];

let options = {
	id: 223524726,
	autoplay: true,
	portrait: false,
	title: false,
	byline: false,
	loop: false,
};

$('.js-video-button').on('click', (e) => {
	let $parent = $(e.currentTarget).parent();
	let container = $(e.currentTarget).siblings('.video')[0];
	let video = new Player(container, options);

	videoObjects.push(video);
	$parent.addClass('is-inited');
});
