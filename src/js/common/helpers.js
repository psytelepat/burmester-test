export let helpers = {
	checkMobile() {
		if (/Mobi/.test(navigator.userAgent)) {
			$('html').addClass('is-device-mobile');
		} else {
			$('html').addClass('is-device-desktop');
		}
	},

	checkIE() {
		let ua = window.navigator.userAgent;
		let msie = ua.indexOf('MSIE ');

		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv:11\./)) {
			$('html').addClass('is-browser-ie');
		}
	},

	isElementPartiallyInViewport(el, yOffset = $(el).height() / 2) {
		let rect = el.getBoundingClientRect();
		let windowHeight = window.innerHeight || document.documentElement.clientHeight;
		let windowWidth = window.innerWidth || document.documentElement.clientWidth;

		let vertInView = rect.top <= windowHeight - yOffset && rect.top + rect.height >= 0;
		let horInView = rect.left <= windowWidth && rect.left + rect.width >= 0;

		return vertInView && horInView;
	},

	isElementInViewport(el) {
		let rect = el.getBoundingClientRect();
		let windowHeight = window.innerHeight || document.documentElement.clientHeight;
		let windowWidth = window.innerWidth || document.documentElement.clientWidth;

		return (
			rect.left >= 0
			&& rect.top >= 0
			&& rect.left + rect.width <= windowWidth
			&& rect.top + rect.height <= windowHeight
		);
	},
};
